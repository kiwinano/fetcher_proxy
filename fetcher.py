# coding: utf8

"""代理采集器
"""

import re
import socket
import urllib2
import os
from urlparse import *
from RegexHelper import RegexHelper

from config import (
    PROXY_SITES, PROXY_DEST, UA,
    PROXY_REGX, FETCH_TIMEOUT
)


class Fetcher(object):

    """代理采集器
    """

    def __init__(self, proxy_dest=PROXY_DEST):
        self.proxy_dest = proxy_dest
        self.fetch_sites = PROXY_SITES
        self.timeout = FETCH_TIMEOUT
        self.rh = RegexHelper()

    def fetch(self):
        """采集代理
        """
        for site in self.fetch_sites:
            self.singlePageFetch(site,site)
        print "done!"

    def singlePageFetch(self,site,url):
        request = urllib2.Request(url)
        request.add_header("User-Agent", UA)
        try:
            print "fetching %s" % site
            response = urllib2.urlopen(
                request, timeout=self.timeout).read()
            proxies = re.findall(self.fetch_sites[site][0], response)

        except (urllib2.URLError, socket.error) as exc:
            print "[ERROR] fetch %s failed: %s" % (site, str(exc))
            proxies = []
            return
        else:
            print "[OK] %s proxies from %s" % (len(proxies), site)
        self.output(proxies, self.fetch_sites[site][1])
        pageUrl = self.rh.matchSingleField(response,self.fetch_sites[site][2])
        if pageUrl:
            if "http" not in pageUrl:
                pageUrl = urlparse(site).scheme + "://" + urlparse(site).netloc + pageUrl
            self.singlePageFetch(site,pageUrl)



    def output(self, proxies,replaceStr):
        """输出
        @proxies, list, 代理列表
        """
        if not proxies:
            return
        with open(self.proxy_dest, "a") as fpt:
            for proxy in proxies:
                if(replaceStr !=''):
                    proxy = re.sub(replaceStr,":",proxy)#proxy.replace(replaceStr,':')
                fpt.write("%s\n" % proxy)


def main():
    """ main """
    fetcher = Fetcher()
    if os.path.isfile(fetcher.proxy_dest):
        os.remove(fetcher.proxy_dest)
    fetcher.fetch()

if __name__ == "__main__":
    main()
