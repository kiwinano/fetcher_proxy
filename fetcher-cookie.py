# coding: utf8

"""代理采集器
"""

import re
import socket
import urllib2
import requests
import os
from urlparse import *
from RegexHelper import RegexHelper
from urllib import unquote
import cookielib
import chardet
import sys
import StringIO,gzip
from config import (
    PROXY_SITES, PROXY_DEST, UA,
    PROXY_REGX, FETCH_TIMEOUT
)


class Fetcher(object):

    """代理采集器
    """

    def __init__(self, proxy_dest=PROXY_DEST):
        self.proxy_dest = proxy_dest
        self.fetch_sites = PROXY_SITES
        self.timeout = FETCH_TIMEOUT
        self.rh = RegexHelper()

    def fetch(self):
        """采集代理
        """
        for site in self.fetch_sites:
            self.singlePageFetch(site,site)
        print "done!"

    def make_cookie(self,nam, value):
        return cookielib.Cookie(
            version=0,
            name=nam,
            value=value,
            port=None,
            port_specified=False,
            domain="www.freeproxylists.net",
            domain_specified=True,
            domain_initial_dot=False,
            path="/",
            path_specified=True,
            secure=False,
            expires=None,
            discard=False,
            comment=None,
            comment_url=None,
            rest=None
        )

    def singlePageFetch(self,site,url):
        # # 声明一个CookieJar对象实例来保存cookie
        # cookie = cookielib.CookieJar()
        # #cookie.set_cookie(self.make_cookie("name","value"))
        # cookie_handler = urllib2.HTTPCookieProcessor(cookie)
        # proxy_handler = urllib2.ProxyHandler({"http": "127.0.0.1:8888"})
        # opener = urllib2.build_opener(proxy_handler,cookie_handler)
        #
        # urllib2.install_opener(opener)
        #
        # headers = {'Host': 'www.freeproxylists.net',
        #            'Connection': 'keep-alive',
        #            'Cache-Control': 'max-age=0',
        #            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
        #            'X-Requested-With': 'XMLHttpRequest',
        #            'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.89 Safari/537.36',
        #            'Upgrade-Insecure-Requests': '1',
        #            'Accept-Encoding': 'gzip, deflate, sdch',
        #            'Accept - Language': 'zh - CN, zh;q = 0.8'
        #            }
        #
        # data = None
        # request = urllib2.Request(url,data,headers)
        # request.add_header("User-Agent", UA)





        try:
            print "fetching %s" % site
           # response = urllib2.urlopen(
           #     request, timeout=self.timeout).read()
            proxies = {"http": "http://127.0.0.1:8888",}
            headers = {'Host': 'www.freeproxylists.net',
                       'Connection': 'keep-alive',
                       'Cache-Control': 'max-age=0',
                       'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
                       'X-Requested-With': 'XMLHttpRequest',
                       'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.89 Safari/537.36',
                       'Upgrade-Insecure-Requests': '1',
                       'Accept-Encoding': 'gzip, deflate, sdch',
                       'Accept - Language': 'zh - CN, zh;q = 0.8'
                       }
            response = requests.get(url,proxies=proxies,headers=headers)

            # compressedstream = StringIO.StringIO(response.text)
            # gziper = gzip.GzipFile(fileobj=compressedstream)
            # data2 = gziper.read()  # 读取解压缩后数据


            proxies = re.findall(self.fetch_sites[site][0], response.text)

        except (urllib2.URLError, socket.error) as exc:
            print "[ERROR] fetch %s failed: %s" % (site, str(exc))
            proxies = []
            return
        else:
            print "[OK] %s proxies from %s" % (len(proxies), site)
        self.output(proxies, self.fetch_sites[site][1])
        pageUrl = self.rh.matchSingleField(response,self.fetch_sites[site][2])
        if pageUrl:
            if "http" not in pageUrl:
                pageUrl = urlparse(site).scheme + "://" + urlparse(site).netloc + pageUrl
            self.singlePageFetch(site,pageUrl)



    def output(self, proxies,replaceStr):
        """输出
        @proxies, list, 代理列表
        """
        if not proxies:
            return
        with open(self.proxy_dest, "a") as fpt:
            for proxy in proxies:
                if(replaceStr !=''):
                    proxy = re.sub(replaceStr,":",proxy)#proxy.replace(replaceStr,':')
                ip = proxy.split(":")[0];
                port = proxy.split(":")[1];
                ip = unquote(ip);
                dr = re.compile(r'<[^>]+>', re.S)
                ip = dr.sub('', ip);
                proxy = ip + ":" + port;
                fpt.write("%s\n" % proxy)


def main():
    """ main """
    fetcher = Fetcher()
    if os.path.isfile(fetcher.proxy_dest):
        os.remove(fetcher.proxy_dest)
    fetcher.fetch()

if __name__ == "__main__":
    main()
