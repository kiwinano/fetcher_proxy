# -*- coding: UTF-8 -*-
import re
from urlparse import urlparse

class RegexHelper(object):
    def getDomain(self,urla):
        domain = urlparse(urla).netloc
        if (domain.startswith("www")):
            REGX = r"www\.(.*?)\."
        else:
            REGX = r"(.*?)\."
        Domain_RE = re.compile(REGX)
        m = Domain_RE.search(domain)
        items = m.groups()
        channel_name = str(items[0])
        channel_name = re.sub("[^0-9a-z]", "", channel_name.lower())
        return channel_name


    def matchSingleField(self,text,regx):
        result=""
        if not text:
            return result
        RE = re.compile(regx)
        m = RE.search(text)
        if m:
            items = m.groups()
            for i in range(len(items)):
                if(i < len(items) -1):
                    result = result + items[i] + "||"
                else:
                    result = result + items[i]
        return result;
#------------------------------------------
#  test
#------------------------------------------
def test_getDomain():
    str = "http://123-ABCc.com"
    rh = RegexHelper()
    channel_name = rh.getDomain(str)
    print channel_name


def test_matchSingleField():
    str1='sdfsdfa<span class="price">11123.00</span>sdfsafadf'
    str2='<span class="price">([^<]*)</span>'
    rh = RegexHelper()
    print rh.matchSingleField(str1,str2)

def test_split():
    str = "PI000001111||http://sfosofsoofs.com"
    id = str.split("||")[0]
    url = str.split("||")[1]
    print id + " " + url


if __name__ == '__main__':
    #@test_getDomain()
    #test_matchSingleField()
    test_split()