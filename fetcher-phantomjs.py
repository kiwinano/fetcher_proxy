# coding: utf8

"""代理采集器
"""

import re
import socket
import urllib2
import uuid

import MySQLdb
import requests
import os
#os.chdir('C:\\Python27\\Lib\\site-packages\\pytesseract')
from urlparse import *

from selenium.webdriver.common.proxy import ProxyType

from RegexHelper import RegexHelper
from urllib import unquote
import cookielib
import chardet
import sys
import StringIO,gzip
from selenium import webdriver
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from time import sleep, time
from PIL import Image
import io
import pytesseract
import random
import datetime

from config import (
    PROXY_SITES, PROXY_DEST, UA,USER_AGENT_LIST,
    PROXY_REGX, FETCH_TIMEOUT
)


class Fetcher(object):

    """代理采集器
    """

    def __init__(self, proxy_dest=PROXY_DEST):
        self.proxy_dest = proxy_dest
        self.fetch_sites = PROXY_SITES
        self.timeout = FETCH_TIMEOUT
        self.rh = RegexHelper()

    def fetch(self):
        """采集代理
        """
        for site in self.fetch_sites:
            self.singlePageFetch(site,self.fetch_sites[site][2])
        print "done!"

    def make_cookie(self,nam, value):
        return cookielib.Cookie(
            version=0,
            name=nam,
            value=value,
            port=None,
            port_specified=False,
            domain="www.freeproxylists.net",
            domain_specified=True,
            domain_initial_dot=False,
            path="/",
            path_specified=True,
            secure=False,
            expires=None,
            discard=False,
            comment=None,
            comment_url=None,
            rest=None
        )

    def singlePageFetch(self,site,pageText,page = 1):
        try:
            print "fetching %s page:%s" % (site,str(page));
            if (page <=1):
                if ( 'proxylists'  in site or 'hidemy.name' in site):
                    # chrome
                    self.driver = webdriver.Chrome()
                    # proxy = webdriver.Proxy()
                    # proxy.proxy_type = ProxyType.DIRECT
                    # proxy.add_to_capabilities(webdriver.DesiredCapabilities.PHANTOMJS)
                    #self.driver.start_session(webdriver.DesiredCapabilities.PHANTOMJS)
                else:
                    #chrome
                    PROXY = "127.0.0.1:1080"  # IP:PORT or HOST:PORT
                    chrome_options = webdriver.ChromeOptions()
                    chrome_options.add_argument('--proxy-server=%s' % PROXY)
                    self.driver = webdriver.Chrome(chrome_options=chrome_options)
                    # proxy = webdriver.Proxy()
                    # proxy.proxy_type = ProxyType.MANUAL
                    # proxy.http_proxy = '127.0.0.1:1080'
                    # proxy.add_to_capabilities(webdriver.DesiredCapabilities.PHANTOMJS)
                    #self.driver.start_session(webdriver.DesiredCapabilities.PHANTOMJS)

                # #phantoms
                # service_args = [
                #     '--proxy=127.0.0.1:1080',
                #     '--proxy-type=socks5',
                # ]
                # if('xroxy' in site or 'proxylists' in site):
                #     service_args =[]
                # # 这是一些配置 关闭loadimages可以加快速度 但是第二页的图片就不能获取了打开(默认)
                # cap = webdriver.DesiredCapabilities.PHANTOMJS
                # cap["phantomjs.page.settings.resourceTimeout"] = 1000
                # cap["phantomjs.page.settings.loadImages"] = False
                # # cap["phantomjs.page.settings.localToRemoteUrlAccessEnabled"] = True
                # self.driver = webdriver.PhantomJS(desired_capabilities=cap)

                try:
                    self.driver.get(site)
                except:
                    return
                #提交
                if('gatherproxy' in site):
                    try:
                        self.driver.find_element_by_class_name('button').click();
                        WebDriverWait(self.driver, 10).until(EC.presence_of_element_located((By.ID, "footer")))
                        self.driver.find_element_by_id('Uptime').send_keys('80');
                        self.driver.find_element_by_xpath('//*[@id="body"]/div[1]/form').submit();
                        WebDriverWait(self.driver, 10).until(EC.presence_of_element_located((By.ID, "footer")))

                    except :
                        return
                if('proxy-listen' in site):
                    try:
                        self.driver.find_element_by_id('downtime').send_keys('>= 90% ');
                        self.driver.find_element_by_id('proxies').send_keys('300');
                        self.driver.find_element_by_id('submit').click();
                        WebDriverWait(self.driver, 10).until(EC.presence_of_element_located((By.ID, "footer")))
                    except :
                        return
            else:
                if('%d' in pageText):
                    pageText = re.sub('%d',str(page),pageText);
                try:
                    self.driver.execute_script('scrollTo(0,document.body.scrollHeight);')
                    sleep(5)
                    linkEle = self.driver.find_element_by_link_text(pageText)
                    linkEle.click()

                except(Exception ):
                    self.driver.quit()
                    return
            text=self.driver.find_element_by_xpath('/html')
            html = text.get_attribute('outerHTML')
            proxies = re.findall(self.fetch_sites[site][0],html )

        except (urllib2.URLError, socket.error,Exception) as exc:
            print "[ERROR] fetch %s failed: %s" % (site, str(exc))
            proxies = []
            return
        else:
            print "[OK] %s proxies from %s" % (len(proxies), site)
        self.output(proxies, self.fetch_sites[site][1])

        page += 1;
        if(page > 50):
            return
        self.singlePageFetch(site,self.fetch_sites[site][2],page)

    def isPageLoad(self):
        return self.driver.execute_script("return document.readyState") =="complete";

    def scroll(driver):
        driver.execute_script("""
            (function () {
                var y = document.body.scrollTop;
                var step = 100;
                window.scroll(0, y);


                function f() {
                    if (y < document.body.scrollHeight) {
                        y += step;
                        window.scroll(0, y);
                        setTimeout(f, 50);
                    }
                    else {
                        window.scroll(0, y);
                        document.title += "scroll-done";
                    }
                }


                setTimeout(f, 1000);
            })();
            """)
    def output(self, proxies,replaceStr):
        """输出
        @proxies, list, 代理列表
        """
        if not proxies:
            return
        with open(self.proxy_dest, "a") as fpt:
            for proxy in proxies:
                #替换冒号
                if(replaceStr !=''):
                    proxy = re.sub(replaceStr,":",proxy)#proxy.replace(replaceStr,':')
                #去掉display：none
                dr_a = re.compile(r'<[^>]*style="display: none[^>]*>[^<]*<[^>]*>', re.S)
                proxy = dr_a.sub('', proxy);
                #去掉hmtl<>
                dr = re.compile(r'<[^>]+>', re.S)
                proxy = dr.sub('', proxy);

                ip = proxy.split(":")[0];
                port = proxy.split(":")[1];
                #处理图片IP
                if(ip.endswith('jpg') or ip.endswith('png')):
                    ip = self.recogenizeImg(ip)
                ip = unquote(ip);

                proxy = ip.strip() + ":" + port.strip();
                fpt.write("%s\n" % proxy)
                #写入数据库
                db = MySQLdb.connect("10.0.1.241", "pbwdata", "8c6ZygzVrmQYcisO", "cpdb")
                cursor = db.cursor()
                # sql = "select count(*) from all_collect_proxyip where  ip_port='%s'" % ( proxy)
                # count = cursor.execute(sql)
                # row = cursor.fetchone()
                # if (row[0] <= 0):
                guid= str(uuid.uuid1())
                sql = "insert into all_collect_proxyip (guid,ip_port,collect_date,url) values('%s','%s','%s','%s')" % (guid,proxy,  datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),'')
                count = cursor.execute(sql)
                db.commit();

    def recogenizeImg(self,url):
        '''
        识别图片中的ip
        :param url:
        :return:
        '''
        url = "https://www.torvpn.com" + url
        PROXY = "127.0.0.1:1080"  # IP:PORT or HOST:PORT
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument('--proxy-server=%s' % PROXY)
        imgDriver= webdriver.Chrome(chrome_options=chrome_options)
        imgDriver.get(url)
        isSaveimage= imgDriver.get_screenshot_as_file('temp.png')
        #image_file = io.BytesIO(fd.read())
        if(isSaveimage):
            im = Image.open('temp.png')
            im.load()
            im.split()
            ip = pytesseract.image_to_string(im,lang='LAN')
        imgDriver.close()
        return ip
def main():
    """ main """

    fetcher = Fetcher()

    #清除原来的不可用的IP
    db = MySQLdb.connect("10.0.1.241", "pbwdata", "8c6ZygzVrmQYcisO", "cpdb")
    cursor = db.cursor()
    #当前时间-24小时
    d = datetime.datetime.now()
    oneday = datetime.timedelta(days=1)
    day = d - oneday
    sql = "delete from all_collect_proxyip where collect_date <'" + day.strftime("%Y-%m-%d %H:%M:%S")  +"'"
    count = cursor.execute(sql)
    db.commit();

    # if os.path.isfile(fetcher.proxy_dest):
    #     os.remove(fetcher.proxy_dest)
    fetcher.fetch()

if __name__ == "__main__":
    main()
